# Wind Factory

La siguiente nota es una guía concisa para la instalación del software _Wind Factory_.

## Requerimientos de Hardware
1. Ya que serán dos contenedores los que se estarán ejecutando en paralelo, el servidor debe contar con dos núcleos de procesador **libres**.
2. La suma de ambos contenedores requiere al menos 2GB de memoria RAM **libres**.
3. Para el almacenamiento de ambos proyectos, imágenes, entre otras cosas necesarias para el correcto funcionamiento del proyecto se sugiere que el servidor cuente con al menos 2GB de disco duro **libres**.

## Requerimientos de Software
**Nota aclaratoria: Wind Factory no requiere de la instalación de Windows Internet Information Services (IIS)**

#### Instalación de Docker Engine
Wind Factory fue pensado para ser ejecutado en contenedores de _Docker_, por lo que el servidor solo necesita que se le instale el motor de _Docker_ (para su instalación se puede encontrar la documentación en la [página oficial de Docker](https://docs.docker.com/engine/install/), también puede visitar la siguiente [página](https://computingforgeeks.com/how-to-run-docker-containers-on-windows-server-2019/) para una instalación en Windows Server 2019 o sino también se puede visitar el siguiente [video](https://www.youtube.com/watch?v=kH5WAM_wkgI)).

#### Instalación de Docker Compose
También es necesario que la instalación de Docker Compose para hacer posible la convivencia de los dos contenedores que crearemos para el correcto funcionamineto de Wind Factory (para su instalación se puede encontrar la documentación en la [página oficial de Docker](https://docs.docker.com/compose/install/)).

## Pasos para la instalación de Wind Factory
1. Descargar el proyecto actual.
2. Dentro del directorio del proyecto actual descargar el editor de tipos de Wind Factory, el cuál puede ser encontrado en el siguiente [link](https://gitlab.com/Marcelo2m97/wind-factory-types-editor).
3. Dentro del directorio del proyecto actual descrgar la API de Wind Factory, la cuál puede ser encontrado en el siguiente [link](https://gitlab.com/Marcelo2m97/wind-factory-api).
4. Editar el archivo __.env.example__.
5. Cambiar el nombre del archivo __.env.example__ por __.env__.
6. Ejecutar desde el directorio del proyecto actual el siguiente comando:
```console
$ docker-compose build
```
7. Ejecutar desde el directorio del proyecto actual el siguiente comando:
```console
$ docker-compose up -d
```
8. Esperar unos minutos a que el servicio se levante.
9. El software ha sido instalado. Para probarlo, en el navegador escribir _localhost:3000_.

